(function () {
    "use strict";
    
    angular.module('controllers.utils', [])
    // ***************** UtilsAppCtrl ************************
    .controller('UtilsAppCtrl', function ($compile, $scope, $http, $timeout, BackendService) {

    	$scope.flagHideAll = false;
    	$scope.flagDisplaySpinner = false;

    	$scope.modal = {
    		alert : {
    			modalClass : '',
    			label : '',
    			body : ''
    		},
    		confirm : {
    			modalClass : 'modal-dialog',
    			label : 'Подтвердите действие',
    			body : '',
    			callback : function () {}
    		}
    	};

    	// применение функции ко всем прорисованным тултипам & поповерам
        $timeout(function() {
            angular.element($('[data-toggle="tooltip"]')).tooltip({
                delay: { "show": 100, "hide": 100 },
                trigger : 'hover'
            });
            angular.element($('[data-toggle="popover"]')).popover({
                delay: { "show": 1000, "hide": 100 },
                placement : 'auto',
                trigger : 'hover'
            });
        },1000);

        $scope.postRequest = function(url, data, callback) {
            $scope.flagDisplaySpinner = true;
            $http.post(url, data).
                success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                    $scope.flagDisplaySpinner = false;
                    $scope.successfullMessage(status, data);
                    if (callback) {
                    	callback(null, data, status, headers, config);
                    };
                }).
                error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                	$scope.flagDisplaySpinner = false;
                	$scope.errorMessage(status, data);
                	if (callback) {
                		callback('error', data, status, headers, config);
                	};
                });
        };

        $scope.getRequest = function(url, callback) {
            $scope.flagDisplaySpinner = true;
            $http.get(url).
                success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                    $scope.flagDisplaySpinner = false;
                    $scope.successfullMessage(status, data);
                    if (callback) {
                    	callback(null, data, status, headers, config);
                    };
                }).
                error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                	$scope.flagDisplaySpinner = false;
                	$scope.errorMessage(status, data);
                	if (callback) {
                		callback('error', data, status, headers, config);
                	};
                });
        };

        $scope.deleteRequest = function(url, callback) {
            $scope.flagDisplaySpinner = true;
            $http.delete(url).
                success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                    $scope.flagDisplaySpinner = false;
                    $scope.successfullMessage(status, data);
                    if (callback) {
                    	callback(null, data, status, headers, config);
                    };
                }).
                error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                	$scope.flagDisplaySpinner = false;
                	$scope.errorMessage(status, data);
                	if (callback) {
                		callback('error', data, status, headers, config);
                	};
                });
        };

        $scope.successfullMessage = function(status, data) {
            // console.log("RESULT code - " + status + " ; data - " + data);
            switch (status) {
                case 200 :
                    $scope.modalAlert();
                    break;
                case 203 :
                    $scope.modalAlert('modal-dialog modal-sm', 'Уже существует !');
                    break;
                case 204 :
                    $scope.modalAlert('modal-dialog modal-sm', 'Нет данных');
                    break;
                default :
                	$scope.modalAlert('modal-dialog', 'Статус - ' + status + '. ' + data);
                	break;
            }
        };

        $scope.errorMessage = function(status, data) {
            $scope.modalAlert('modal-dialog', 'Ошибка - ' + status + '. ' + data);
        };

        $scope.modalAlert = function(modalClass, body, label) {
        	if (modalClass) {
        		$scope.modal.alert.modalClass = modalClass;
        	} else {
        		$scope.modal.alert.modalClass = 'modal-dialog modal-sm';
        	};
        	if (label) {
        		$scope.modal.alert.label = label;
        	} else {
        		$scope.modal.alert.label = 'Результат запроса';
        	};
        	if (body) {
        		$scope.modal.alert.body = body;
        	} else {
        		$scope.modal.alert.body = 'Выполнено успешно !';
        	};
        	$('#modalAlert').modal('show');
        };

        $scope.modalConfirm = function(body, callback, label, modalClass) {
        	if (modalClass) {
        		$scope.modal.confirm.modalClass = modalClass;
        	} else {
        		$scope.modal.confirm.modalClass = 'modal-dialog';
        	};
        	if (label) {
        		$scope.modal.confirm.label = label;
        	} else {
        		$scope.modal.confirm.label = 'Подтвердите действие';
        	};
        	$scope.modal.confirm.body = body;
        	$scope.modal.confirm.callback = callback;
            $('#modalConfirm').modal('show');
        };

	});

}());