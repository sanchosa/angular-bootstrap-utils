### Angular-utils - 1.0 

Работа пакета основана на вложенных контроллерах angular и тимплейтах swig. Используются компоненты bootstrap.



### Как использовать : 
* Необходимо добавить в основной модуль зависимость - 
 		
```
#!js

'controllers.utils'
```
 
* 	На страницу добавить -
 		
```
#!js

{% block head %}
 	<link href="/css/utils.css" rel="stylesheet">
{% endblock %}
```

 
* 		Подключить как родительский контроллер - 
 			
```
#!js

ng-controller="UtilsAppCtrl"
```

 
* 		В любом месте страницы добавить - 
 			
```
#!swig

{% include 'utils.swig.html' %}
```
 
 
* 		Подключить модуль утилит  - 
 			
```
#!js

<script src="/js/controller/utils.js"></script>
```


### Функции утилит в дочернем контроллере могут быть вызваны через $scope.имя функции.

### Доступные функции - 
*	При заранее заданном классе .ng-hide и ng-hide="flagHideAll" в первом блоке прорисовывает содержимое страницы после инициализации ангулара.
*	Инициализирует все элементы страницы data-toggle="tooltip" и data-toggle="popover".
*	$scope.postRequest = function(url, data, [callback]) - Отправляет пост запрос отображая спиннер во время выполнения. По завершении отображает сообщения о результате и выполняет callback функцию если задана.
*	$scope.getRequest = function(url, [callback]) - аналогично $scope.postRequest.
*	$scope.deleteRequest = function(url, [callback]) - аналогично $scope.postRequest.
*	$scope.errorMessage = function(status, data) - Выводит модальный бутстрэп алерт с ошибкой.
*	$scope.modalAlert = function([modalClass], [body], [label]) - Выводит модальный бутстрэп алерт.
*	$scope.modalConfirm = function(body, callback, [label], [modalClass]) - Выводит модальный бутстрэп confirm, после подтверждения выполняет функцию callback.